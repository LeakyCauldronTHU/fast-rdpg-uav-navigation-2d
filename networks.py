try:
        import tensorflow.compat.v1 as tf
        tf.disable_v2_behavior()
except:
        import tensorflow as tf
import numpy as np
import math

def fanin_init(shape,fanin=None):
  fanin = fanin or shape[0]
  v = 1/np.sqrt(fanin)
  return tf.random_uniform(shape,minval=-v,maxval=v)


l1 = 400 # dm 400
l2 = 300 # dm 300

def theta_p(state_dim,action_dim,name='policy'):
  w1 = tf.Variable(fanin_init([state_dim,l1]),name='w1')
  b1 = tf.Variable(fanin_init([l1],state_dim),name='b1')
  w2 = tf.Variable(fanin_init([l1+action_dim,l2]),name='w2')
  b2 = tf.Variable(fanin_init([l2],l1+action_dim),name='b2')
  
  with tf.variable_scope(name):
    lstm_cell = tf.contrib.rnn.BasicLSTMCell(l2, forget_bias=0.0, state_is_tuple=True)
    state = lstm_cell.zero_state(1, "float")
    (cell_output, final_state) = tf.nn.dynamic_rnn(
      cell=lstm_cell,sequence_length=None,inputs=tf.placeholder(tf.float32,[1,None,l2]),initial_state=state,dtype=tf.float32)
    lstm_variables = [v for v in tf.global_variables() if v.name.startswith(name)]
    
  w3 = tf.Variable(tf.random_uniform([l2,action_dim],-3e-3,3e-3),name='w3')
  b3 = tf.Variable(tf.random_uniform([action_dim],-3e-3,3e-3),name='b3')
  return [w1,b1,w2,b2]+lstm_variables+[w3,b3]

def policy_network(state,action,theta,state_dim,action_dim,name='policy'):
  #print("state_shape",np.shape(state))
  #print("action_shape",np.shape(action))
  with tf.variable_scope(name):
    with tf.name_scope('hidden_layers_in_'+name+'_network'):
      h0 = tf.identity(state,name='h0-state')
      #print("h0",np.shape(h0))
      h1 = tf.nn.relu( tf.matmul(h0,theta[0]) + theta[1],name='h1')
      #print("h1",np.shape(h1))
      #print("theta[2]",np.shape(theta[2]))
      h2 = tf.nn.relu( tf.matmul(tf.concat([h1,action],1),theta[2]) + theta[3],name='h2')
      print("h2",np.shape(h2))
      h2 = tf.expand_dims(h2,0)
      print("h2",np.shape(h2))
      
    with tf.name_scope('lstm_layer_in'+name+'_network'):
      lstm_cell = tf.contrib.rnn.BasicLSTMCell(l2, forget_bias=0.0, state_is_tuple=True,reuse=True)
      state = lstm_cell.zero_state(1, dtype=tf.float32)
      (out_put, state) = tf.nn.dynamic_rnn(\
        cell=lstm_cell,sequence_length=None,inputs=h2,initial_state=state,dtype=tf.float32)
      print("out_put",np.shape(out_put))
      out_put = out_put[:,-1,:]
      print('out_put',np.shape(out_put))
        
    with tf.name_scope('output_layer_in'+name+'_network'):
      h3 = tf.identity(tf.matmul(out_put,theta[6]) + theta[7],name='h3')
      action = tf.nn.tanh(h3,name='action')
      print("action_output",np.shape(action))
    return action


def theta_q(state_dim,action_dim,name='critic'):
  w1 = tf.Variable(fanin_init([state_dim,l1]),name='w1')
  b1 = tf.Variable(fanin_init([l1],state_dim),name='b1')
  w2 = tf.Variable(fanin_init([l1+action_dim,l2]),name='w2')
  b2 = tf.Variable(fanin_init([l2],l1+action_dim),name='b2')
  
  with tf.variable_scope(name):
    lstm_cell = tf.contrib.rnn.BasicLSTMCell(l2, forget_bias=0.0, state_is_tuple=True)
    state = lstm_cell.zero_state(1, "float")
    (cell_output, final_state) = tf.nn.dynamic_rnn(\
      cell=lstm_cell,sequence_length=None,inputs=tf.placeholder(tf.float32,[1,None,l2]),initial_state=state,dtype=tf.float32)
    lstm_variables = [v for v in tf.global_variables() if v.name.startswith(name)]
    
  w3 = tf.Variable(tf.random_uniform([l2,1],-3e-3,3e-3),name='w3')
  b3 = tf.Variable(tf.random_uniform([1],-3e-3,3e-3),name='b3')
  return [w1,b1,w2,b2]+lstm_variables+[w3,b3]
  

def q_network(state,action,theta,state_dim,action_dim,name='critic'):
  #print("state_shape",np.shape(state))
  #print("action_shape",np.shape(action))
  with tf.variable_scope(name):
    with tf.name_scope('hidden_layers_in_'+name+'_network'):
      h0 = tf.identity(state,name='h0-state')
      #print("h0",np.shape(h0))
      h1 = tf.nn.relu( tf.matmul(h0,theta[0]) + theta[1],name='h1')
      #print("h1",np.shape(h1))
      #print("theta[2]",np.shape(theta[2]))
      h2 = tf.nn.relu( tf.matmul(tf.concat([h1,action],1),theta[2]) + theta[3],name='h2')
      print("h2",np.shape(h2))
      h2 = tf.expand_dims(h2,0)
      print("h2",np.shape(h2))
      
    with tf.name_scope('lstm_layer_in'+name+'_network'):
      lstm_cell = tf.contrib.rnn.BasicLSTMCell(l2, forget_bias=0.0, state_is_tuple=True,reuse=True)
      state = lstm_cell.zero_state(1, dtype=tf.float32)
      (out_put, state) = tf.nn.dynamic_rnn(
        cell=lstm_cell,sequence_length=None,inputs=h2,initial_state=state,dtype=tf.float32)
      out_put = out_put[:,-1,:]
      print('out_put',np.shape(out_put))
        
    with tf.name_scope('output_layer_in'+name+'_network'):
      critic = tf.identity(tf.matmul(out_put,theta[6]) + theta[7],name='critic')
      print("critc_output",np.shape(critic))
    return critic




                                             
