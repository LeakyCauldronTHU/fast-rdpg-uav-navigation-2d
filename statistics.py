from Env import EnvUAV
from gym_ddpg import config
from ddpg import DDPG


env = EnvUAV()
agent = DDPG(env,config)
state_dim = env.num_states
action_dim = env.num_actions
Count = 0

for i in range(300):
      state_sequence = []
      action_sequence = []
      state = env.reset()
      state_sequence.append(state)
      action_sequence.append(np.zeros([action_dim]))
      num_steps = 0
      done = False
      print('testing episode', i)
      while not (done):
            num_steps += 1
            #env.render()
            if num_steps > config.NUM_STEPS:
                  action = agent.test_action(\
                        state_sequence[num_steps-config.NUM_STEPS:],\
                        action_sequence[num_steps-config.NUM_STEPS:])
            else:
                  action = agent.test_action(state_sequence,action_sequence)
            #print(action)
            next_state,reward,done,done2 = env.forward(action)
            state_sequence.append(next_state)
            action_sequence.append(action)
            if done:
                  Count += 1
                  break
            if num_steps > 5000:
                  break
      print('Count:',Count)
      print('num_steps',num_steps)
        
        
        




    

