try:
        import tensorflow.compat.v1 as tf
        tf.disable_v2_behavior()
except:
        import tensorflow as tf
num_states=10
batch=5
num_steps=20
sess = tf.InteractiveSession()
deep_output = tf.zeros([batch,num_steps,num_states])
def LSTM_var():
      with tf.variable_scope("lstm_critic_target") as LCT:
            lstm_cell = tf.contrib.rnn.BasicLSTMCell(num_states, forget_bias=0.0, state_is_tuple=True)
            cell = tf.contrib.rnn.MultiRNNCell([lstm_cell] *1, state_is_tuple=True)
            state = cell.zero_state(batch, dtype=tf.float32)
            for i in range(num_steps):
                  if i >0:  tf.get_variable_scope().reuse_variables()
                  (cell_output, state) = cell(deep_output[:,i,:],state)
      lstm_variables = [v for v in tf.global_variables()]
      return lstm_variables
      
def LSTM():
      with tf.variable_scope("lstm_critic_target") as LCT:
            lstm_cell = tf.contrib.rnn.BasicLSTMCell(num_states, forget_bias=0.0, state_is_tuple=True,reuse=True)
            cell = tf.contrib.rnn.MultiRNNCell([lstm_cell] *1, state_is_tuple=True)
            state = cell.zero_state(batch, dtype=tf.float32)
            for i in range(num_steps):
                  if i >0:  tf.get_variable_scope().reuse_variables()
                  (cell_output, state) = cell(deep_output[:,i,:],state)
            '''
                  lstm_cell = tf.contrib.rnn.BasicLSTMCell(num_states, forget_bias=0.0, state_is_tuple=True)
                  cell = tf.contrib.rnn.MultiRNNCell([lstm_cell] *1, state_is_tuple=True)
                  state = cell.zero_state(batch, dtype=tf.float32)
            '''
      
      lstm_variables = [v for v in tf.global_variables()]
      print(lstm_variables)


def main():
      
      var = LSTM_var()
      print(var)
      LSTM()
      LSTM()


if __name__ == '__main__':
    main()
