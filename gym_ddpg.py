from Env import EnvUAV
from ddpg import DDPG
import numpy as np
try:
        import tensorflow.compat.v1 as tf
        tf.disable_v2_behavior()
except:
        import tensorflow as tf

flags = tf.app.flags
FLAGS = flags.FLAGS

"network parameters"
flags.DEFINE_integer('layer1_size',400,'nodes number of the first layer')
flags.DEFINE_integer('layer2_size',300,'nodes number of the second layer')

"learning parameters"
flags.DEFINE_integer('replay_buffer_size',30000,'size of the replay buffer')
flags.DEFINE_integer('replay_start_size',10000,'minimum replay buffer size for training')## stage two
flags.DEFINE_integer('batch_size',8,'batch_size')
flags.DEFINE_integer('NUM_STEPS',25,'maximum episode length')
flags.DEFINE_integer('EPISODES',100000,'episodes for training')
flags.DEFINE_integer('TEST',200,'number of episodes for test')
flags.DEFINE_float('q_learning_rate',0.001,'learning rate for critic network')
flags.DEFINE_float('p_learning_rate',0.0001,'learning rate fot policy network')
flags.DEFINE_float('tau',0.001,'updating rate of target network')
flags.DEFINE_float('gamma',0.99,'discount factor')

"saving parameters"


class config():
    layer1_size = FLAGS.layer1_size
    layer2_size = FLAGS.layer2_size

    replay_buffer_size = FLAGS.replay_buffer_size
    replay_start_size = FLAGS.replay_start_size
    batch_size = FLAGS.batch_size
    NUM_STEPS = FLAGS.NUM_STEPS
    EPISODES = FLAGS.EPISODES
    TEST = FLAGS.TEST
    q_learning_rate = FLAGS.q_learning_rate
    p_learning_rate = FLAGS.p_learning_rate
    tau = FLAGS.tau
    gamma = FLAGS.gamma
    

def main():
    env = EnvUAV()
    agent = DDPG(env,config)
    state_dim = env.num_states
    action_dim = env.num_actions
    train_step = 0
    start_training = 0
    SAVE_RATE = 1000
    Flag = False
    for episode in range(config.EPISODES):
        state_sequence = []
        action_sequence = []
        reward_sequence = []
        done_sequence = []
        num_steps = 0
        state = env.reset()
        state_sequence.append(state)
        action_sequence.append(np.zeros([action_dim]))
        episode_rewards = []
        # print("**********************  (stochasic_50)start collecting episode:",episode+1)
        # Train
        for step in range(config.NUM_STEPS):
            train_step += 1
            #env.render()
            action = agent.noise_action(state_sequence,action_sequence)
            state_sequence = state_sequence[0:step+1]
            action_sequence = action_sequence[0:step+1]
            next_state,reward,done,_ = env.forward(action)
            state_sequence.append(next_state)
            action_sequence.append(action)
            agent.perceive(state_sequence,action_sequence,[reward],[done])
            episode_rewards.append(reward)
            num_steps = step+1
            if train_step % SAVE_RATE == 0:
                # agent.save()
                File_reward = open('./Result/reward.txt', 'a')
                File_reward.write(str(train_step))
                File_reward.write(' ')
                File_reward.write(str(np.mean(episode_rewards)))
                File_reward.write('\n')
                File_reward.close()
                episode_rewards = []
                
            if done:
                break
        if (train_step > config.replay_start_size) and (not start_training):
            start_training = episode
            Flag = True
            
        # File_reward = open('./Result/reward.txt','a')
        # # Testing:
        # if (episode-start_training) % 100 == 0 and Flag:
        #     # print("#################################################")
        #     # print("&&&&&&&&&&&&&&&&&&&& testing %%%%%%%%%%%%%%%%%%%%")
        #     # print("#################################################")
        #     total_reward = 0
        #     for epi in range(config.TEST):
        #         # print("################### collecting test steps ",epi+1," #################")
        #         state_sequence = []
        #         action_sequence = []
        #         state = env.reset()
        #         state_sequence.append(state)
        #         action_sequence.append(np.zeros([action_dim]))
        #         for step in range(100):
        #             #env.render()
        #             if step+1 > config.NUM_STEPS:
        #                 action = agent.test_action(state_sequence[step+1-config.NUM_STEPS:],action_sequence[step+1-config.NUM_STEPS:])
        #             else:
        #                 action = agent.test_action(state_sequence,action_sequence)
        #
        #             state_sequence = state_sequence[0:step+1]
        #             action_sequence = action_sequence[0:step+1]
        #             #print("action in step",step,action)
        #             next_state,reward,done,_ = env.forward(action)
        #             action_sequence.append(action)
        #             state_sequence.append(next_state)
        #             total_reward += reward
        #             if done:
        #                 break
        #
        #     ave_reward = total_reward/config.TEST
        #     File_reward.write(str(ave_reward))
        #     File_reward.write(' ')
        #     File_reward.write(str(episode-start_training))
        #     File_reward.write('\n')
        #     # print('episode: ',epi,'Evaluation Average Reward:',ave_reward)
        # File_reward.close()
    

if __name__ == '__main__':
    main()




    

