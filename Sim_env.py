from Env import EnvUAV
import numpy as np
Environment = EnvUAV()
Environment.reset()
num_actions = Environment.num_actions
action = 2*(np.random.rand(1)-0.5)
done = False
'''
while(True):
        Environment.reset()
        Environment.render()
'''
while(True):
        while(not done):
                action = 2*(np.random.rand(1)-0.5)
                _, _, done = Environment.step(action)
                Environment.render()
        flag = int(input('continue to simulate the environment?(1/0)'))

        if flag == 1:
                done = False
                action = 2*(np.random.rand(1)-0.5)
                Environment.reset()
        else:
                print('environment simulation ends')
                break

