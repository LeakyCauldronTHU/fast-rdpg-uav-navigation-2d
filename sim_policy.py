from Env import EnvUAV
from gym_ddpg import config
from ddpg import DDPG
import os

env = EnvUAV()
agent = DDPG(env,config)
state_dim = env.num_states
action_dim = env.num_actions
Count = 0

while(True):
      os.remove('./Result/path.txt')
      Count += 1
      state_sequence = []
      action_sequence = []
      state = env.reset()
      state_sequence.append(state)
      action_sequence.append(np.zeros([action_dim]))
      num_steps = 0
      done = False
      while not (done):
            num_steps += 1
            env.render()
            if num_steps > config.NUM_STEPS:
                  action = agent.test_action(\
                        state_sequence[num_steps-config.NUM_STEPS:],\
                        action_sequence[num_steps-config.NUM_STEPS:])
            else:
                  action = agent.test_action(state_sequence,action_sequence)
            print(action)
            next_state,reward,done,_ = env.forward(action)
            state_sequence.append(next_state)
            action_sequence.append(action)
            
      flag = int(input('continue to simulate the policy?(1/0)'))
      if flag == 1:
            print('continue next episode')
      else:
            print('policy simulation ends')
            break
        
        
        




    

