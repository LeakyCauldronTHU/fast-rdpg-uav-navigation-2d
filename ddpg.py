# -----------------------------------
# Deep Deterministic Policy Gradient
# Author: Flood Sung
# Date: 2016.5.4
# -----------------------------------
try:
        import tensorflow.compat.v1 as tf
        tf.disable_v2_behavior()
except:
        import tensorflow as tf
import numpy as np
import random
from collections import deque
from ou_noise import OUNoise
import networks

# Hyper Parameters:
L2_Q = 0.01
L2_POLICY = 0.0

class Normalizer(object):
    def __init__(self):
        self.mean = -4
        self.std = 20
        self.decay_rate = 0.99

    def normalize(self, reward):
        self.mean = self.decay_rate * self.mean + (1. - self.decay_rate) * reward.mean()
        self.std = self.decay_rate * self.std + (1. - self.decay_rate) * reward.std()
        return self.mean, self.std
    
class DDPG:
        def __init__(self, env, config):
                # Load environment
                self.name = 'DDPG'
                self.reward_normalizer = Normalizer()
                self.environment = env
                state_dim = env.num_states
                action_dim = env.num_actions
                # Initialize network and learning hyper-parameters
                self.replay_buffer_size = config.replay_buffer_size
                self.replay_start_size = config.replay_start_size
                self.num_steps = config.NUM_STEPS
                self.batch_size = config.batch_size
                self.gamma = config.gamma
                self.layer1_size = config.layer1_size
                self.layer2_size = config.layer2_size
                self.q_learning_rate = config.q_learning_rate
                self.p_learning_rate = config.p_learning_rate
                self.tau = config.tau
                # Initialize time step
                self.time_step = 0
                # initialize replay buffer
                self.replay_buffer = deque()
                # initialize networks
                self.create_networks_and_training_method(state_dim,action_dim)

                self.sess = tf.InteractiveSession()
                self.sess.run(tf.global_variables_initializer())
                self.sess.run(self.init_p)
                self.sess.run(self.init_q)
                print("*********target network initialized************")

                # loading networks
                self.saver = tf.train.Saver()
                checkpoint = tf.train.get_checkpoint_state("saved_networks")
                if checkpoint and checkpoint.model_checkpoint_path:
                                self.saver.restore(self.sess, checkpoint.model_checkpoint_path)
                                print("Successfully loaded:", checkpoint.model_checkpoint_path)
                else:
                                print("Could not find old network weights")

                global summary_writer
                summary_writer = tf.summary.FileWriter('./logs',graph=self.sess.graph)
        
        def create_networks_and_training_method(self,state_dim,action_dim):

                theta_p = networks.theta_p(state_dim,action_dim,'policy')
                theta_q = networks.theta_q(state_dim,action_dim,'critic')
                target_theta_p = networks.theta_p(state_dim,action_dim,'target_policy')
                target_theta_q = networks.theta_q(state_dim,action_dim,'target_critic')
                print(tf.global_variables())
                target_p_init = []
                for idx in range(len(target_theta_p)): target_p_init.append(target_theta_p[idx].assign(theta_p[idx]))
                
                self.init_p = tf.group(target_p_init[0],target_p_init[1],
                                       target_p_init[2],target_p_init[3],
                                       target_p_init[4],target_p_init[5],
                                       target_p_init[6],target_p_init[7])

                target_q_init = []
                for idx in range(len(target_theta_q)): target_q_init.append(tf.assign(target_theta_q[idx],theta_q[idx]))
                
                self.init_q = tf.group(target_q_init[0],target_q_init[1],
                                       target_q_init[2],target_q_init[3],
                                       target_q_init[4],target_q_init[5],
                                       target_q_init[6],target_q_init[7])

                with tf.name_scope('Input_of_states_and_action'):
                        self.state = tf.placeholder(tf.float32,[None,state_dim],'state') #self.batch_size*self.num_steps*state_dims
                        self.action = tf.placeholder(tf.float32,[None,action_dim],'action')
                        print("self.state",np.shape(self.state))
                        print("self.action",np.shape(self.action))
                with tf.name_scope('action_test'):
                        self.action_test = networks.policy_network(self.state[0:-1,:],self.action[0:-1,:],theta_p,state_dim,action_dim,'policy')
                        self.action_comp = networks.policy_network(self.state[:,:],self.action[:,:],theta_p,state_dim,action_dim,'policy')

                with tf.name_scope('action_exploration'):
                        # Initialize a random process the Ornstein-Uhlenbeck process for action exploration
                        self.exploration = OUNoise(action_dim)
                        #noise = self.exploration.noise()
                        noise = tf.truncated_normal([1],0,0.25)
                        noise = np.random.uniform(-0.25,0.25)
                        self.action_exploration = self.action_test + noise
                        print("noise",np.shape(noise))
                        print("self.action_test",np.shape(self.action_test))
                        print("self.action_exploration",np.shape(self.action_exploration))

                with tf.name_scope('critic_output_for_actor_training'):
                        q = networks.q_network(self.state[0:-1,:],
                                               tf.concat([self.action[1:-1,:],self.action_test],0),theta_q,state_dim,action_dim,'critic')
                        # policy optimization
                        print("q",np.shape(q))

                with tf.name_scope('loss_of_policy'):
                        mean_q = tf.reduce_mean(q)
                        weight_decay_p = tf.add_n([L2_POLICY * tf.nn.l2_loss(var) for var in theta_p])  
                        loss_p = -mean_q + weight_decay_p

                with tf.name_scope('optimizing_policy'):
                        optim_p = tf.train.AdamOptimizer(self.p_learning_rate)
                        self.grads_and_vars_p = optim_p.compute_gradients(loss_p, var_list=theta_p)
                        self.gradients_p = []
                        for i in range(len(self.grads_and_vars_p)):
                                self.gradients_p.append([tf.placeholder(tf.float32,np.shape(self.grads_and_vars_p[i][0])),theta_p[i]])
                        optimize_p = optim_p.apply_gradients(self.gradients_p)

                with tf.name_scope('updating_target_policy'):
                        target_update_p = []
                        for idx in range(len(theta_p)):
                                target_update_p.append(tf.assign(target_theta_p[idx],self.tau*theta_p[idx]+(1-self.tau)*target_theta_p[idx]))
                        with tf.control_dependencies([optimize_p]):
                                self.train_p = tf.group(target_update_p[0],target_update_p[1],
                                                        target_update_p[2],target_update_p[3],
                                                        target_update_p[4],target_update_p[5],
                                                        target_update_p[6],target_update_p[7])
                
                # q optimization
                #self.action_train = tf.placeholder(tf.float32,[None,action_dim],'action_train')
                with tf.name_scope('Input_of_reward_and_done'):
                        self.reward = tf.placeholder(tf.float32,[1],'reward')
                        self.done = tf.placeholder(tf.bool,[1],'done')
                        self.reward_mean = tf.placeholder(tf.float32,[None],'reward_mean')
                        self.reward_std = tf.placeholder(tf.float32,[None],'reward_std')

                with tf.name_scope('output_of_critic'):
                        q_train = networks.q_network(self.state[0:-1,:],self.action[1:,:],
                                                     theta_q,state_dim,action_dim,'critic')

                with tf.name_scope('next_action_and_next_critic'):
                        next_action = networks.policy_network(self.state[:,:],self.action[:,:],
                                                              target_theta_p,state_dim,action_dim,'target_policy')
                        print("next_action",np.shape(next_action))
                        next_q = networks.q_network(self.state[:,:],tf.concat([self.action[1:,:],next_action],0),\
                                                    target_theta_q,state_dim,action_dim,'target_critic')
                        print("next_q",np.shape(next_q))#self.batch_size*self.num_steps*1

                with tf.name_scope('calculating_target_q'):
                        q_target = tf.where(self.done,self.reward,self.reward + self.gamma * next_q[0])

                with tf.name_scope('loss_of_critic'):
                        print("q_target",np.shape(q_target))
                        print("q_train",np.shape(q_train))
                        q_error = tf.reduce_mean(tf.square(q_target - q_train[0]))
                        print("q_error",np.shape(q_error))
                        weight_decay_q = tf.add_n([L2_Q * tf.nn.l2_loss(var) for var in theta_q])
                        loss_q = q_error + weight_decay_q

                with tf.name_scope('optimizing_critic'):
                        optim_q = tf.train.AdamOptimizer(self.q_learning_rate)
                        self.grads_and_vars_q = optim_q.compute_gradients(loss_q, var_list=theta_q)
                        self.gradients_q = []
                        for i in range(len(self.grads_and_vars_q)):
                                self.gradients_q.append([tf.placeholder(tf.float32,np.shape(self.grads_and_vars_q[i][0])),theta_q[i]])
                        optimize_q = optim_q.apply_gradients(self.gradients_q)

                with tf.name_scope('updating_target_critic'):
                        target_update_q = []
                        for idx in range(len(theta_q)): target_update_q.append(tf.assign(target_theta_q[idx],self.tau*theta_q[idx]+(1-self.tau)*target_theta_q[idx]))
                        with tf.control_dependencies([optimize_q]):
                                self.train_q = tf.group(target_update_q[0],target_update_q[1],
                                                        target_update_q[2],target_update_q[3],
                                                        target_update_q[4],target_update_q[5],
                                                        target_update_q[6],target_update_q[7])
                

                tf.summary.scalar("loss_q",loss_q)
                tf.summary.scalar("loss_p",loss_p)
                tf.summary.scalar("q_mean",mean_q)
                tf.summary.scalar("reward_mean",tf.reduce_sum(self.reward_mean))
                tf.summary.scalar("reward_std",tf.reduce_sum(self.reward_std))
                
                global merged_summary_op
                merged_summary_op = tf.summary.merge_all()

        def train(self, reward_mean, reward_std):
                #print "train step",self.time_step
                # Randomly sample a sequence from replay buffer
                grads_batch_p = []
                grads_batch_q = []
                grads_update_p = []
                grads_update_q = []
                for bat in range(self.batch_size):
                        minibatch = random.sample(self.replay_buffer,1)
                        state_batch = [data[0] for data in minibatch][0]
                        action_batch = [data[1] for data in minibatch][0]
                        reward_batch = [data[2] for data in minibatch][0]
                        done_batch = [data[3] for data in minibatch][0]
                        '''
                        print("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&")
                        print("state_batch",np.shape(state_batch))
                        print("action_batch",np.shape(action_batch))
                        print("reward_batch",np.shape(reward_batch))
                        print("done_batch",np.shape(done_batch))
                        print("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&")
                        '''

                        grads_p,grads_q,summary_str = self.sess.run([self.grads_and_vars_p,self.grads_and_vars_q,merged_summary_op],feed_dict={
                                self.state:state_batch,
                                self.action:action_batch,
                                self.reward:reward_batch,
                                self.done:done_batch,
                                self.reward_mean: [reward_mean],
                                self.reward_std: [reward_std]
                                })
                        grads_batch_p.append(grads_p)
                        grads_batch_q.append(grads_q)
                        summary_writer.add_summary(summary_str,self.time_step-self.replay_start_size)
                # Calculate gradients for updating
                for i in range(len(grads_batch_p[0])):
                        grad_temp = []
                        for j in range(self.batch_size):
                                grad_temp.append(grads_batch_p[j][i][0])
                        grads_update_p.append(np.mean(grad_temp,0))

                for i in range(len(grads_batch_q[0])):
                        grad_temp = []
                        for j in range(self.batch_size):
                                grad_temp.append(grads_batch_q[j][i][0])
                        grads_update_q.append(np.mean(grad_temp,0))

                self.sess.run([self.train_p,self.train_q],feed_dict={
                        self.gradients_p[0][0]:grads_update_p[0],
                        self.gradients_p[1][0]:grads_update_p[1],
                        self.gradients_p[2][0]:grads_update_p[2],
                        self.gradients_p[3][0]:grads_update_p[3],
                        self.gradients_p[4][0]:grads_update_p[4],
                        self.gradients_p[5][0]:grads_update_p[5],
                        self.gradients_p[6][0]:grads_update_p[6],
                        self.gradients_p[7][0]:grads_update_p[7],
                        self.gradients_q[0][0]:grads_update_q[0],
                        self.gradients_q[1][0]:grads_update_q[1],
                        self.gradients_q[2][0]:grads_update_q[2],
                        self.gradients_q[3][0]:grads_update_q[3],
                        self.gradients_q[4][0]:grads_update_q[4],
                        self.gradients_q[5][0]:grads_update_q[5],
                        self.gradients_q[6][0]:grads_update_q[6],
                        self.gradients_q[7][0]:grads_update_q[7]
                        })

                # save network every 1000 iteration
                if self.time_step % 600 == 0:
                        self.saver.save(self.sess, 'saved_networks/' + 'network' + '-ddpg', global_step = self.time_step-self.replay_start_size)

        def noise_action(self,state,action):
                action = self.sess.run(self.action_comp,feed_dict={
                        self.state:state, self.action:action
                        })
                noise = np.array([np.random.uniform(-0.25,0.25),np.random.uniform(-0.5,0.5)])
                # print('action',action)
                action = action[0]+noise
                return np.clip(action,self.environment.action_space_low,self.environment.action_space_high)
        
        def test_action(self,state,action):
                action = self.sess.run(self.action_comp,feed_dict={
                        self.state:state, self.action:action
                        })[0]
                return action
        
        def perceive(self,state_sequence,action_sequence,reward,done):
                reward_mean, reward_std = self.reward_normalizer.normalize(reward[0])
                # Store transition (s_t,a_t,r_t,s_{t+1}) in replay buffer
                self.replay_buffer.append((state_sequence,action_sequence,reward,done))
                # Update time step
                self.time_step += 1

                # Limit the replay buffer size
                if len(self.replay_buffer) > self.replay_buffer_size:
                        self.replay_buffer.popleft()

                # Store transitions to replay start size then start training
                if self.time_step > self.replay_start_size:
                        self.train(reward_mean, reward_std)
                        #print('######################### training finished ########################')

                # Re-iniitialize the random process when an episode ends
                if done:
                        self.exploration.reset()


        

