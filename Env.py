# coding: utf-8
''' UAV_environment '''
import numpy as np
import copy

class EnvUAV():
    num_states = 14
    num_actions = 2
    action_space_low = -1
    action_space_high = 1

    
    def __init__(self):
        self.File = open('./Result/path.txt','w+')        
        ##无人机参数
        self.level = 90.0 #定义无人机的飞行高度
        self.scope = 100.0 #定义无人机传感器的视野范围
        self.step_size = 2.0 #定义无人机单步步长
        self.state = np.random.uniform(0, 1, size = (14,)) #定义无人机状态向量并随机初始化
        self.reward = 0.0
        #self.position #记录无人机的当前位置
        #self.target #记录无人机的目标位置
        #self.orient #[-1,1]记录无人机的第一视角方向

        ##环境参数
        #self.mat_height
        #self.mat_exist
        self.max_dist = 4000.0 #定义无人机距离目标的最大距离
        self.Flag = 0 #用来标记选用的是那一种环境
        self.num_square = 4#3 #square环境重复次数
        self.num_circle = 8#8#20 #circle环境重复次数
        self.num_triangle = 12#6#15 #triangle环境重复次数
        self.num_tile = 5#4#10 #tile环境重复次数
        self.num_crossing = 8#5#15 #crossing环境重复次数
        self.step = 0.1 #定义无人机传感器搜索步长

    def range_finder1(self,position,target,theta_vertical,theta,num):
        end_cache = copy.deepcopy(position)
        end = copy.deepcopy(position)
        end = np.mod(end,250)
        self.state[num] = 1.0
        for j in range(0,1000):
            position_integer = end_cache/250
            if (self.mat_height[int(position_integer[0]) + 8,int(position_integer[1]) + 8]-self.level>0) and \
               (end[0] >= 25 and end[0] <= 225 and end[1] >= 25 and end[1] <= 225):
                self.state[num] = np.linalg.norm(end_cache-position)/self.scope
                break
            end_cache[0] = end_cache[0] + self.step*np.sin(theta)
            end_cache[1] = end_cache[1] + self.step*np.cos(theta)
            end = copy.deepcopy(end_cache)
            end = np.mod(end,250)
        return

    def range_finder2(self,position,target,theta_vertical,theta,num):
        end_cache = copy.deepcopy(position)
        end = copy.deepcopy(position)
        end = np.mod(end,80)
        self.state[num] = 1.0
        for j in range(0,1000):
            position_integer = end_cache/80
            if (self.mat_height[int(position_integer[0]) + 8,int(position_integer[1]) + 8]-self.level>0) and \
               (np.sqrt(np.power(end[0]-40,2) + np.power(end[1]-40,2))-30 <= 0):
                self.state[num] = np.linalg.norm(end_cache-position)/self.scope
                break
            end_cache[0] = end_cache[0] + self.step * np.sin(theta)
            end_cache[1] = end_cache[1] + self.step * np.cos(theta)
            end = copy.deepcopy(end_cache)
            end = np.mod(end,80)
        return

    def range_finder3(self,position,target,theta_vertical,theta,num):
        end_cache = copy.deepcopy(position)
        end = copy.deepcopy(position)
        end = np.mod(end,120)
        self.state[num] = 1.0
        for j in range(0,1000):
            position_integer = end_cache/120
            if (self.mat_height[int(position_integer[0]) + 8,int(position_integer[1]) + 8]-self.level>0) and \
               ((end[0] >= 20 and end[0] <= 100 and end[1] >= 20 and end[1] <= 100)
                and not ((end[0]<40 and end[1]<40) or (end[0]<40 and end[1]>80)
                         or (end[0]>80 and end[1]<40) or (end[0]>80 and end[1]>80))):
                self.state[num] = np.linalg.norm(end_cache-position)/self.scope
                break
            end_cache[0] = end_cache[0] + self.step * np.sin(theta)
            end_cache[1] = end_cache[1] + self.step * np.cos(theta)
            end = copy.deepcopy(end_cache)
            end = np.mod(end,120)
        return

    def range_finder4(self,position,target,theta_vertical,theta,num):
        end_cache = copy.deepcopy(position)
        end = copy.deepcopy(position)
        end = np.mod(end,200)
        self.state[num] = 1.0
        for j in range(0,1000):
            position_integer = end_cache/200
            if (self.mat_height[int(position_integer[0]) + 8,int(position_integer[1]) + 8]-self.level>0) and \
               ((end[0] >= 20 and end[0] <= 180 and end[1] >= 20 and end[1] <= 180)
                and not ( (end[0] >= 20 and end[0] <= 60 and end[1] >= 80 and end[1] <= 120)
                          or (end[0] >= 140 and end[0] <= 180 and end[1] >= 80 and end[1] <= 120)
                          or (end[0] >= 80 and end[0] <= 120 and end[1] >= 20 and end[1] <= 60)
                          or (end[0] >= 80 and end[0] <= 120 and end[1] >= 140 and end[1] <= 180))):
                self.state[num] = np.linalg.norm(end_cache-position)/self.scope
                break
            end_cache[0] = end_cache[0] + self.step * np.sin(theta)
            end_cache[1] = end_cache[1] + self.step * np.cos(theta)
            end = copy.deepcopy(end_cache)
            end = np.mod(end,200)
        return
            
    def obtain_state(self, position, target, orient):
        jobs = []
        if self.Flag == 1: #square
            for i in range(0,9):
                theta = np.mod((i-4)*np.pi/8 + orient,2*np.pi) #计算传感器各个方向的角度,保证不超过360度
                self.range_finder1(position, target, 0, theta, i)
                
        #################################################################################################
        #################################################################################################            
        elif self.Flag == 2: #circle
            for i in range(0,9):
                theta = np.mod((i-4)*np.pi/8 + orient,2*np.pi) #计算传感器各个方向的角度,保证不超过360度
                self.range_finder2(position, target, 0, theta, i)

        #################################################################################################
        #################################################################################################            
        elif self.Flag == 3: #crossing
            for i in range(0,9):
                theta = np.mod((i-4)*np.pi/8 + orient,2*np.pi) #计算传感器各个方向的角度,保证不超过360度
                self.range_finder3(position, target, 0, theta, i)

        #################################################################################################
        ################################################################################################# 
        else:
            for i in range(0,9):
                theta = np.mod((i-4)*np.pi/8 + orient,2*np.pi) #计算传感器各个方向的角度,保证不超过360度
                self.range_finder4(position, target, 0, theta, i)
        

        #计算当前位置距离目标的距离
        dist  =  np.linalg.norm(target-position)
        self.state[9] = 2/(np.exp(-0.002*dist) + 1)-1

        #计算目标和当前位置的相对夹角
        theta_target = np.arctan((target[0]-position[0])/(target[1]-position[1]))
        if (target[0] >= position[0] and target[1] >= position[1]):
            self.state[10] = np.sin(theta_target)
            self.state[11] = np.cos(theta_target)
        elif target[1]<position[1]:
            self.state[10] = np.sin(theta_target + np.pi)
            self.state[11] = np.cos(theta_target + np.pi)
        else:
            self.state[10] = np.sin(theta_target + 2*np.pi)
            self.state[11] = np.cos(theta_target + 2*np.pi)

        #保存目标的绝对方向角
        self.state[12] = np.sin(orient) #normalization
        self.state[13] = np.cos(orient)

    def reset(self):
        self.Flag = np.random.random_integers(4)
        #print('Flag',self.Flag)
        #self.Flag  =  3 #仅仅测试时使用
        #################################################################################################
        #################################################################################################
        if self.Flag == 1: #square
            self.mat_height = np.random.randint(1,10,size = (self.num_square + 16,self.num_square + 16))*17.0 + 30.0 #定义环境中建筑物的高度

            p1_x = np.random.uniform(0, 250)
            p1_y = np.random.uniform(0, 25)

            p2_x = np.random.uniform(0, 250)
            p2_y = np.random.uniform(225, 250)

            p3_x = np.random.uniform(0, 25)
            p3_y = np.random.uniform(25, 225)

            p4_x = np.random.uniform(225, 250)
            p4_y = np.random.uniform(25, 225)

            choice = np.random.uniform(0,1)
            if choice <= 25.0/90:
                position  =  [p1_x,p1_y]
            elif choice <= 50.0/90:
                position  =  [p2_x,p2_y]
            elif choice <= 70.0/90:
                position  =  [p3_x,p3_y]
            else:
                position  =  [p4_x,p4_y]

            relative_position = np.floor(np.random.uniform(0, self.num_square, size = (2,))) #####floor函数是否可用
            self.position = np.array(position)  +  relative_position*250  #计算得到初始位置
            

            ###############################其次产生目标位置#####################################
            p1_x = np.random.uniform(0, 250)
            p1_y = np.random.uniform(0, 25)

            p2_x = np.random.uniform(0, 250)
            p2_y = np.random.uniform(225, 250)

            p3_x = np.random.uniform(0, 25)
            p3_y = np.random.uniform(25, 225)

            p4_x = np.random.uniform(225, 250)
            p4_y = np.random.uniform(25, 225)

            choice_tar = np.random.uniform(0,1)
            if choice_tar <= 25.0/90:
                target  =  [p1_x,p1_y]
            elif choice_tar <= 50.0/90:
                target  =  [p2_x,p2_y]
            elif choice_tar <= 70.0/90:
                target  =  [p3_x,p3_y]
            else:
                target  =  [p4_x,p4_y]

            relative_target = np.floor(np.random.uniform(0, self.num_square, size = (2,))) #####floor函数是否可用
            self.target  =  np.array(target)  +  relative_target*250

        #################################################################################################
        #################################################################################################    
        elif self.Flag == 2: #circle
            self.mat_height = np.random.randint(1,10,size = (self.num_circle + 16,self.num_circle + 16))*17.0 + 30.0 #定义环境中建筑物的高度

            while 1:
                x = np.random.uniform(0,80)
                y = np.random.uniform(0,80)
                if np.sqrt(np.power(x-40,2) + np.power(y-40,2))-30>0:
                    position = [x,y]
                    break

            relative_position = np.floor(np.random.uniform(0, self.num_circle, size = (2,))) #####floor函数是否可用
            self.position = np.array(position)  +  relative_position*80  #计算得到初始位置
            
            ###############################其次产生目标位置#####################################
            while 1:
                x = np.random.uniform(0,80)
                y = np.random.uniform(0,80)
                if np.sqrt(np.power(x-40,2) + np.power(y-40,2))-30>0:
                    target = [x,y]
                    break

            relative_target = np.floor(np.random.uniform(0, self.num_circle, size = (2,))) #####floor函数是否可用
            self.target  =  np.array(target)  +  relative_target*80

        #################################################################################################
        #################################################################################################    
        elif self.Flag == 3: #crossing
            self.mat_height = np.random.randint(1,10,size = (self.num_crossing + 16,self.num_crossing + 16))*17.0 + 30.0 #定义环境中建筑物的高度

            p1_x = np.random.uniform(0, 120)
            p1_y = np.random.uniform(0, 20)

            p2_x = np.random.uniform(0, 120)
            p2_y = np.random.uniform(100, 120)

            p3_x = np.random.uniform(0, 20)
            p3_y = np.random.uniform(20, 100)

            p4_x = np.random.uniform(100, 120)
            p4_y = np.random.uniform(20, 100)

            p5_x = np.random.uniform(0,20)
            p5_y = np.random.uniform(0,20)

            choice = np.random.uniform(0,1)
            if choice <= 1.0/6:
                choice = np.random.uniform(0,1)
                if choice <= 0.25:
                    position  =  np.array([p5_x,p5_y]) + [20,20]
                elif choice <= 0.5:
                    position  =  np.array([p5_x,p5_y]) + [20,80]
                elif choice <= 0.75:
                    position  =  np.array([p5_x,p5_y]) + [80,20]
                else:
                    position  =  np.array([p5_x,p5_y]) + [80,80]
            else:
                choice = np.random.uniform(0,1)
                if choice <= 6.0/22:
                    position  =  [p1_x,p1_y]
                elif choice <= 12.0/22:
                    position  =  [p2_x,p2_y]
                elif choice <= 17.0/22:
                    position  =  [p3_x,p3_y]
                else:
                    position  =  [p4_x,p4_y]

            relative_position = np.floor(np.random.uniform(0, self.num_crossing, size = (2,))) #####floor函数是否可用
            self.position = np.array(position)  +  relative_position*120  #计算得到初始位置
            
            #################################其次产生目标位置#####################################
            p1_x = np.random.uniform(0, 120)
            p1_y = np.random.uniform(0, 20)

            p2_x = np.random.uniform(0, 120)
            p2_y = np.random.uniform(100, 120)

            p3_x = np.random.uniform(0, 20)
            p3_y = np.random.uniform(20, 100)

            p4_x = np.random.uniform(100, 120)
            p4_y = np.random.uniform(20, 100)

            p5_x = np.random.uniform(0,20)
            p5_y = np.random.uniform(0,20)

            choice = np.random.uniform(0,1)
            if choice <= 1.0/6:
                choice = np.random.uniform(0,1)
                if choice <= 0.25:
                    target  =  np.array([p5_x,p5_y]) + [20,20]
                elif choice <= 0.5:
                    target  =  np.array([p5_x,p5_y]) + [20,80]
                elif choice <= 0.75:
                    target  =  np.array([p5_x,p5_y]) + [80,20]
                else:
                    target  =  np.array([p5_x,p5_y]) + [80,80]
            else:
                choice = np.random.uniform(0,1)
                if choice <= 6.0/22:
                    target  =  [p1_x,p1_y]
                elif choice <= 12.0/22:
                    target  =  [p2_x,p2_y]
                elif choice <= 17.0/22:
                    target  =  [p3_x,p3_y]
                else:
                    target  =  [p4_x,p4_y]

            relative_target = np.floor(np.random.uniform(0, self.num_crossing, size = (2,))) #####floor函数是否可用
            self.target  =  np.array(target)  +  relative_target*120

        #################################################################################################
        #################################################################################################
        else:
            self.mat_height = np.random.randint(1,10,size = (self.num_tile + 16,self.num_tile + 16))*17.0 + 30.0 #定义环境中建筑物的高度

            p1_x = np.random.uniform(0, 200)
            p1_y = np.random.uniform(0, 20)

            p2_x = np.random.uniform(0, 200)
            p2_y = np.random.uniform(180, 200)

            p3_x = np.random.uniform(0, 20)
            p3_y = np.random.uniform(20, 180)

            p4_x = np.random.uniform(180, 200)
            p4_y = np.random.uniform(20, 180)

            p5_x = np.random.uniform(0, 40)
            p5_y = np.random.uniform(0, 40)

            choice = np.random.uniform(0,1)
            if choice  <= 9.0/13:
                if choice <= 25.0/90:
                    position  =  [p1_x,p1_y]
                elif choice <= 50.0/90:
                    position  =  [p2_x,p2_y]
                elif choice <= 70.0/90:
                    position  =  [p3_x,p3_y]
                else:
                    position  =  [p4_x,p4_y]
            else:
                choice = np.random.uniform(0,1)
                if choice <= 0.25:
                    position  =  np.array([p5_x,p5_y]) + [20,80]
                elif choice <= 0.5:
                     position  =  np.array([p5_x,p5_y]) + [80,20]
                elif choice <= 0.75:
                    position  =  np.array([p5_x,p5_y]) + [80,140]
                else:
                    position  =  np.array([p5_x,p5_y]) + [140,80]

            relative_position = np.floor(np.random.uniform(0, self.num_tile, size = (2,))) #####floor函数是否可用
            self.position = np.array(position)  +  relative_position*200  #计算得到初始位置
            
            ###############################其次产生目标位置#####################################
            p1_x = np.random.uniform(0, 200)
            p1_y = np.random.uniform(0, 20)

            p2_x = np.random.uniform(0, 200)
            p2_y = np.random.uniform(180, 200)

            p3_x = np.random.uniform(0, 20)
            p3_y = np.random.uniform(20, 180)

            p4_x = np.random.uniform(180, 200)
            p4_y = np.random.uniform(20, 180)

            p5_x = np.random.uniform(0, 40)
            p5_y = np.random.uniform(0, 40)

            choice = np.random.uniform(0,1)
            if choice  <= 9.0/13:
                if choice <= 25.0/90:
                    target  =  [p1_x,p1_y]
                elif choice <= 50.0/90:
                    target  =  [p2_x,p2_y]
                elif choice <= 70.0/90:
                    target  =  [p3_x,p3_y]
                else:
                    target  =  [p4_x,p4_y]
            else:
                choice = np.random.uniform(0,1)
                if choice <= 0.25:
                    target  =  np.array([p5_x,p5_y]) + [20,80]
                elif choice <= 0.5:
                    target  =  np.array([p5_x,p5_y]) + [80,20]
                elif choice <= 0.75:
                    target  =  np.array([p5_x,p5_y]) + [80,140]
                else:
                    target  =  np.array([p5_x,p5_y]) + [140,80]

            relative_target = np.floor(np.random.uniform(0, self.num_tile, size = (2,))) #####floor函数是否可用
            self.target  =  np.array(target)  +  relative_target*200

        #################################################################################################
        self.orient  =  np.random.uniform(0, 1)*2*np.pi
        #################################################################################################
        #print('Flag',self.Flag,'relative_position ',relative_position)
        np.savetxt('./Result/mat_height.txt',self.mat_height,fmt='%d',delimiter=' ',newline='\r\n')
        #np.savetxt('./Result/mat_exist.txt',self.mat_exist,fmt='%d',delimiter=' ',newline='\r\n')
        self.obtain_state(np.copy(self.position), np.copy(self.target), np.copy(self.orient))
        observation = np.copy(self.state)
        
        return observation

    def forward(self, action):
        # print('position',self.position)
        state_temp = np.copy(self.state)
        position_temp = np.copy(self.position)
        self.orient = np.mod(action[0]*np.pi+self.orient,2*np.pi)
        self.position = self.position + [self.step_size*np.sin(self.orient),self.step_size*np.cos(self.orient)] #按照给定方向向前一步走

        self.obtain_state(np.copy(self.position), np.copy(self.target), np.copy(self.orient))
        next_observation = np.copy(self.state)
        #################################################################################################
        if self.Flag == 1: #square
            relative_position = np.copy(self.position)
            relative_position = np.mod(relative_position,250)
            position_integer = self.position/250
            done1 = (relative_position[0] >= 25 and relative_position[0] <= 225 and relative_position[1] >= 25 and relative_position[1] <= 225)\
                    and (self.mat_height[int(position_integer[0]) + 8,int(position_integer[1]) + 8]-self.level>0)
            done2 = (np.linalg.norm((self.position-self.target)) <= 20)
            
        #################################################################################################       
        elif self.Flag == 2: #circle
            relative_position = np.copy(self.position)
            relative_position = np.mod(relative_position,80)
            position_integer = self.position/80
            done1 = (np.sqrt(np.power(relative_position[0]-40,2) + np.power(relative_position[1]-40,2))-30 <= 0)\
                    and (self.mat_height[int(position_integer[0]) + 8,int(position_integer[1]) + 8]-self.level>0)
            done2 = (np.linalg.norm((self.position-self.target)) <= 20)
        #################################################################################################    
        elif self.Flag == 3: #crossing
            relative_position = np.copy(self.position)
            relative_position = np.mod(relative_position,120)
            position_integer = self.position/120
            done1 = (relative_position[0] >= 20 and relative_position[0] <= 100 and relative_position[1] >= 20 and relative_position[1] <= 100)\
                  and not( (relative_position[0]<40 and relative_position[1]<40) or (relative_position[0]<40 and relative_position[1]>80)\
                           or (relative_position[0]>80 and relative_position[1]<40) or (relative_position[0]>80 and relative_position[1]>80))\
                           and (self.mat_height[int(position_integer[0]) + 8,int(position_integer[1]) + 8]-self.level>0)
            done2 = (np.linalg.norm((self.position-self.target)) <= 20)
        #################################################################################################
        else:
            relative_position = np.copy(self.position)
            relative_position = np.mod(relative_position,200)
            position_integer = self.position/200
            done1 = (relative_position[0] >= 20 and relative_position[0] <= 180 and relative_position[1] >= 20 and relative_position[1] <= 180)\
                   and not ( (relative_position[0] >= 20 and relative_position[0] <= 60 and relative_position[1] >= 80 and relative_position[1] <= 120)\
                             or (relative_position[0] >= 140 and relative_position[0] <= 180 and relative_position[1] >= 80 and relative_position[1] <= 120)\
                             or (relative_position[0] >= 80 and relative_position[0] <= 120 and relative_position[1] >= 20 and relative_position[1] <= 60)\
                             or (relative_position[0] >= 80 and relative_position[0] <= 120 and relative_position[1] >= 140 and relative_position[1] <= 180) )\
                             and (self.mat_height[int(position_integer[0]) + 8,int(position_integer[1]) + 8]-self.level>0)
            done2 = (np.linalg.norm((self.position-self.target)) <= 20)
        #################################################################################################       
        ######################################Specifying Reward##########################################
        #################################################################################################
            
        done = done1 or done2
        reward_sparse = 0
        # #Sparse Reward
        # if done1:
        #     reward_sparse = 0.0
        #     # print('crash down:',relative_position,'\n')
        # if done2:
        #     reward_sparse = 100.0
        #     # print('successfuly arrived:',np.linalg.norm((self.position-self.target)),'\n')
        
        reward_distance = (np.linalg.norm(position_temp-self.target)-np.linalg.norm(self.position-self.target))/self.step_size*2
        
        if np.max(self.state[0:9]) == self.state[4]:
            reward_distance = reward_distance + 0.1
        
        reward_barrier = -8*np.exp(-25*np.min(self.state[0:9]))
        reward_action = -0.6
        #Final Reward
        self.reward = reward_sparse + reward_barrier + reward_distance + reward_action
        return next_observation, np.copy(self.reward), done, done2
    
    def render(self):
        ##输出轨迹参数
        #print('Flag:',self.Flag,' target position:',self.target,' current position: ',self.position)
        self.File.write(str(self.target[0]))
        self.File.write(' ')
        self.File.write(str(self.target[1]))
        self.File.write(' ')
        self.File.write(str(self.position[0]))
        self.File.write(' ')
        self.File.write(str(self.position[1]))
        self.File.write(' ')
        for i in range(14):
            self.File.write(str(self.state[i]))
            self.File.write(' ')
        self.File.write(str(self.Flag))
        self.File.write('\n')
    def env_exit(self):
        self.File.close()
        






